import React, { Component } from 'react';

class Lists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataHargaBuah: [
                { nama: 'Semangka', harga: 10000, berat: 1000 },
                { nama: 'Anggur', harga: 40000, berat: 500 },
                { nama: 'Strawberry', harga: 30000, berat: 400 },
                { nama: 'Jeruk', harga: 30000, berat: 1000 },
                { nama: 'Mangga', harga: 30000, berat: 500 },
            ],
            nama: '',
            harga: '',
            berat: '',
            index: -1,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        var index = this.state.index;
        if (index === -1) {
            this.setState({
                dataHargaBuah: [
                    ...this.state.dataHargaBuah,
                    {
                        nama: this.state.nama,
                        harga: this.state.harga,
                        berat: this.state.berat,
                    },
                ],
                nama: '',
                harga: '',
                berat: '',
            });
        } else {
            var newDataBuah = this.state.dataHargaBuah[index];
            newDataBuah.nama = this.state.nama;
            newDataBuah.harga = this.state.harga;
            newDataBuah.berat = this.state.berat;

            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah],
                nama: '',
                harga: '',
                berat: '',
                index: -1,
            });
        }
    }

    handleRemoveFields(index) {
        this.state.dataHargaBuah.splice(index, 1);
        this.setState({
            dataHargaBuah: [...this.state.dataHargaBuah],
        });
    }

    handleEditForm(index) {
        var peserta = this.state.dataHargaBuah[index];
        this.setState({
            nama: peserta.nama,
            harga: peserta.harga,
            berat: peserta.berat,
            index,
        });
    }

    render() {
        return (
            <div className="holder">
                <h1>Daftar Peserta Lomba</h1>
                <table>
                    <thead>
                        <tr className="head">
                            <th>No</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataHargaBuah.map((val, index) => {
                            return (
                                <tr className="list" key={index}>
                                    <td>{index + 1}</td>
                                    <td>{val.nama}</td>
                                    <td>{val.harga}</td>
                                    <td>{val.berat / 1000} kg</td>
                                    <td>
                                        <button
                                            onClick={() => {
                                                this.handleEditForm(index);
                                            }}
                                        >
                                            edit
                                        </button>
                                        <button
                                            onClick={() => {
                                                this.handleRemoveFields(index);
                                            }}
                                        >
                                            remove
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                {/* Form */}
                <div>
                    <h1>Form Buah</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="input">
                            <label>Masukkan Nama buah:</label>
                            <input
                                type="text"
                                name="nama"
                                value={this.state.nama}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="input">
                            <label>Masukkan Harga buah:</label>
                            <input
                                type="number"
                                name="harga"
                                value={this.state.harga}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="input">
                            <label>Masukkan Berat buah:</label>
                            <input
                                type="number"
                                name="berat"
                                value={this.state.berat}
                                onChange={this.handleChange}
                            />
                        </div>
                        <button style={{ marginTop: 30 }}>submit</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Lists;
