import React from 'react';

const Tugas9 = () => {
    return (
        <div className="holder">
            <div>
                <h1>Form Pembelian Buah</h1>
            </div>
            <div>
                <form className="input">
                    <div className="label">
                        <label for="name">
                            <b>Nama Pelanggan</b>
                        </label>
                    </div>
                    <input type="text" id="name" />
                </form>
                <form className="input">
                    <div className="label" id="item">
                        <label for="namaBuah">
                            <b>Daftar Item</b>
                        </label>
                    </div>
                    <div>
                        <input type="checkbox" />
                        <label for="Semangka">Semangka</label>
                        <br />
                        <input type="checkbox" />
                        <label for="Jeruk">Jeruk</label>
                        <br />
                        <input type="checkbox" />
                        <label for="Nanas">Nanas</label>
                        <br />
                        <input type="checkbox" />
                        <label for="Salak">Salak</label>
                        <br />
                        <input type="checkbox" />
                        <label for="Anggur">Anggur</label>
                    </div>
                </form>
                <button>Kirim</button>
            </div>
        </div>
    );
};

export default Tugas9;
