import React, { Component } from 'react';

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: new Date().toLocaleTimeString(),
            counter: 100,
            showTimer: true,
        };
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start });
            this.setState({ counter: this.props.start });
        }
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    componentDidUpdate() {
        if (this.state.showTimer === true) {
            if (this.state.counter < 0) {
                this.setState({
                    showTimer: false,
                });
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: new Date().toLocaleTimeString(),
            counter: this.state.counter - 1,
        });
    }

    render() {
        return (
            <div className="holder">
                {this.state.showTimer && (
                    <div>
                        <h1 style={{ textAlign: 'center' }}>
                            sekarang jam : {this.state.time}
                        </h1>
                        <h1>hitung mundur : {this.state.counter}</h1>
                    </div>
                )}
            </div>
        );
    }
}

export default Timer;
