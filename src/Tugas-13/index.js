import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Tugas13 = () => {
    const [dataHargaBuah, setDataHargaBuah] = useState(null);
    const [input, setInput] = useState({
        name: '',
        price: '',
        weight: '',
        id: null,
    });

    const handleChangeName = (event) => {
        let value = event.target.value;
        setInput({ ...input, name: value });
    };
    const handleChangePrice = (event) => {
        let value = event.target.value;
        setInput({ ...input, price: value });
    };
    const handleChangeWeight = (event) => {
        let value = event.target.value;
        setInput({ ...input, weight: value });
    };

    useEffect(() => {
        if (dataHargaBuah === null) {
            axios
                .get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then((res) => {
                    setDataHargaBuah(res.data);
                    console.log(res);
                });
        }
    }, [dataHargaBuah]);

    const handleSubmit = (event) => {
        event.preventDefault();
        if (input.id === null) {
            axios
                .post(` http://backendexample.sanbercloud.com/api/fruits`, {
                    name: input.name,
                    price: input.price,
                    weight: input.weight,
                })
                .then((res) => {
                    let data = res.data;
                    setDataHargaBuah([
                        ...dataHargaBuah,
                        {
                            name: data.name,
                            price: data.price,
                            weight: data.weight,
                            id: data.id,
                        },
                    ]);
                    setInput({ name: '', price: '', weight: '', id: null });
                });
        } else {
            axios
                .put(
                    `http://backendexample.sanbercloud.com/api/fruits/${input.id}`,
                    {
                        name: input.name,
                        price: input.price,
                        weight: input.weight,
                    }
                )
                .then((res) => {
                    var newDataBuah = dataHargaBuah.map((x) => {
                        if (x.id === input.id) {
                            x.name = input.name;
                            x.price = input.price;
                            x.weight = input.weight;
                        }
                        return x;
                    });
                    setDataHargaBuah(newDataBuah);
                    setInput({ name: '', price: '', weight: '', id: null });
                });
        }
    };

    const handleEditForm = (event) => {
        let idBuah = parseInt(event.target.value);
        let buah = dataHargaBuah.find((x) => x.id === idBuah);

        setInput({
            id: idBuah,
            name: buah.name,
            price: buah.price,
            weight: buah.weight,
        });
    };

    const handleDelete = (event) => {
        var idBuah = parseInt(event.target.value);
        console.log(idBuah);
        axios
            .delete(
                `http://backendexample.sanbercloud.com/api/fruits/${idBuah}`
            )
            .then((res) => {
                var newDataBuah = dataHargaBuah.filter((x) => x.id !== idBuah);
                console.log(newDataBuah);
                setDataHargaBuah(newDataBuah);
            });
    };

    return (
        <div className="holder">
            <h1>Daftar Peserta Lomba</h1>
            <table>
                <thead>
                    <tr className="head">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataHargaBuah !== null &&
                        dataHargaBuah.map((item, index) => {
                            return (
                                <tr className="list" key={item.id}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{item.weight / 1000} kg</td>
                                    <td>
                                        <button
                                            value={item.id}
                                            onClick={handleEditForm}
                                        >
                                            edit
                                        </button>
                                        <button
                                            value={item.id}
                                            onClick={handleDelete}
                                        >
                                            remove
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
            {/* Form */}
            <div>
                <h1>Form Buah</h1>
                <form>
                    <div className="input">
                        <label>Masukkan Nama buah:</label>
                        <input
                            type="text"
                            value={input.name}
                            onChange={handleChangeName}
                        />
                    </div>
                    <div className="input">
                        <label>Masukkan Harga buah:</label>
                        <input
                            type="number"
                            value={input.price}
                            onChange={handleChangePrice}
                        />
                    </div>
                    <div className="input">
                        <label>Masukkan Berat buah:</label>
                        <input
                            type="number"
                            value={input.weight}
                            onChange={handleChangeWeight}
                        />
                    </div>
                    <button onClick={handleSubmit} style={{ marginTop: 30 }}>
                        submit
                    </button>
                </form>
            </div>
        </div>
    );
};

export default Tugas13;
