import React from 'react';

let dataHargaBuah = [
    { nama: 'Semangka', harga: 10000, berat: 1000 },
    { nama: 'Anggur', harga: 40000, berat: 500 },
    { nama: 'Strawberry', harga: 30000, berat: 400 },
    { nama: 'Jeruk', harga: 30000, berat: 1000 },
    { nama: 'Mangga', harga: 30000, berat: 500 },
];

const Tugas10 = () => {
    return (
        <div className="holder">
            <h1>Tabel Harga Buah</h1>
            <table>
                <thead>
                    <tr className="head">
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                    </tr>
                </thead>
                <tbody>
                    {dataHargaBuah.map((el) => {
                        return (
                            <tr className="list">
                                <td>{el.nama}</td>
                                <td>{el.harga}</td>
                                <td>{el.berat / 1000} kg</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default Tugas10;
