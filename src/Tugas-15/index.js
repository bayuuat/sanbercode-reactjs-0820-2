import React, { useContext } from 'react';
import { ThemeContext, themes } from './Theme';

const Tugas15 = () => {
    const [tema, setTema] = useContext(ThemeContext);

    const handleTema = () => {
        console.log(tema);
        if (tema === themes.dark) {
            setTema(themes.light);
        } else {
            setTema(themes.dark);
        }
    };

    return (
        <div className="holder">
            <button onClick={handleTema}>Change Theme</button>
        </div>
    );
};

export default Tugas15;
