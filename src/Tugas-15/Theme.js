import React, { createContext, useState } from 'react';

export const ThemeContext = createContext();

export const themes = {
    dark: {
        background: 'black',
        color: 'white',
    },
    light: {
        background: 'white',
        color: 'black',
    },
};

export const TemaProvider = (props) => {
    const [tema, setTema] = useState(themes.dark);

    return (
        <ThemeContext.Provider value={[tema, setTema]}>
            {props.children}
        </ThemeContext.Provider>
    );
};
