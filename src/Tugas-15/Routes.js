import React from 'react';
import Tugas9 from '../Tugas-9';
import Tugas10 from '../Tugas-10';
import Tugas11 from '../Tugas-11';
import Tugas12 from '../Tugas-12';
import Tugas13 from '../Tugas-13';
import Tugas14 from '../Tugas-14';
import Tugas15 from '.';
import Nav from './Nav';
import { TemaProvider } from './Theme';
import { Switch, Route } from 'react-router';

const Routes = () => {
    return (
        <TemaProvider>
            <Nav />

            <div className="container">
                <Switch>
                    <Route exact path="/" component={Tugas9} />
                    <Route exact path="/tugas-10" component={Tugas10} />
                    <Route exact path="/tugas-11" component={Tugas11} />
                    <Route exact path="/tugas-12" component={Tugas12} />
                    <Route exact path="/tugas-13" component={Tugas13} />
                    <Route exact path="/tugas-14" component={Tugas14} />
                    <Route exact path="/tugas-15" component={Tugas15} />
                </Switch>
            </div>
        </TemaProvider>
    );
};

export default Routes;
