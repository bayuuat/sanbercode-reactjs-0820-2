import React, { useContext } from 'react';
import { ThemeContext } from './Theme';
import { Link } from 'react-router-dom';

const Nav = () => {
    const [tema] = useContext(ThemeContext);

    return (
        <nav style={tema}>
            <ul>
                <li>
                    <Link to="/">Tugas 9</Link>
                </li>
                <li>
                    <Link to="/tugas-10">Tugas10</Link>
                </li>
                <li>
                    <Link to="/tugas-11">Tugas11</Link>
                </li>
                <li>
                    <Link to="/tugas-12">Tugas12</Link>
                </li>
                <li>
                    <Link to="/tugas-13">Tugas13</Link>
                </li>
                <li>
                    <Link to="/tugas-14">Tugas14</Link>
                </li>
                <li>
                    <Link to="/tugas-15">Tugas15</Link>
                </li>
            </ul>
        </nav>
    );
};

export default Nav;
