import React, { useContext } from 'react';
import { BuahContext } from './BuahContext';
import axios from 'axios';

const BuahForm = () => {
    const [dataHargaBuah, setDataHargaBuah, input, setInput] = useContext(
        BuahContext
    );

    const handleSubmit = (event) => {
        event.preventDefault();
        if (input.id === null) {
            axios
                .post(` http://backendexample.sanbercloud.com/api/fruits`, {
                    name: input.name,
                    price: input.price,
                    weight: input.weight,
                })
                .then((res) => {
                    let data = res.data;
                    setDataHargaBuah([
                        ...dataHargaBuah,
                        {
                            name: data.name,
                            price: data.price,
                            weight: data.weight,
                            id: data.id,
                        },
                    ]);
                    setInput({ name: '', price: '', weight: '', id: null });
                });
        } else {
            axios
                .put(
                    `http://backendexample.sanbercloud.com/api/fruits/${input.id}`,
                    {
                        name: input.name,
                        price: input.price,
                        weight: input.weight,
                    }
                )
                .then((res) => {
                    var newDataBuah = dataHargaBuah.map((x) => {
                        if (x.id === input.id) {
                            x.name = input.name;
                            x.price = input.price;
                            x.weight = input.weight;
                        }
                        return x;
                    });
                    setDataHargaBuah(newDataBuah);
                    setInput({ name: '', price: '', weight: '', id: null });
                });
        }
    };

    const handleChangeName = (event) => {
        let value = event.target.value;
        setInput({ ...input, name: value });
    };

    const handleChangePrice = (event) => {
        let value = event.target.value;
        setInput({ ...input, price: value });
    };

    const handleChangeWeight = (event) => {
        let value = event.target.value;
        setInput({ ...input, weight: value });
    };

    return (
        <>
            <div>
                <h1>Form Buah</h1>
                <form>
                    <div className="input">
                        <label>Masukkan Nama buah:</label>
                        <input
                            type="text"
                            value={input.name}
                            onChange={handleChangeName}
                        />
                    </div>
                    <div className="input">
                        <label>Masukkan Harga buah:</label>
                        <input
                            type="number"
                            value={input.price}
                            onChange={handleChangePrice}
                        />
                    </div>
                    <div className="input">
                        <label>Masukkan Berat buah:</label>
                        <input
                            type="number"
                            value={input.weight}
                            onChange={handleChangeWeight}
                        />
                    </div>
                    <button onClick={handleSubmit} style={{ marginTop: 30 }}>
                        submit
                    </button>
                </form>
            </div>
        </>
    );
};

export default BuahForm;
