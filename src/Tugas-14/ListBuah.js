import React, { useContext } from 'react';
import { BuahContext } from './BuahContext';
import axios from 'axios';

const BuahList = () => {
    const [dataHargaBuah, setDataHargaBuah, setInput] = useContext(BuahContext);

    const handleEditForm = (event) => {
        let idBuah = parseInt(event.target.value);
        let buah = dataHargaBuah.find((x) => x.id === idBuah);

        setInput({
            id: idBuah,
            name: buah.name,
            price: buah.price,
            weight: buah.weight,
        });
    };

    const handleDelete = (event) => {
        var idBuah = parseInt(event.target.value);
        console.log(idBuah);
        axios
            .delete(
                `http://backendexample.sanbercloud.com/api/fruits/${idBuah}`
            )
            .then((res) => {
                var newDataBuah = dataHargaBuah.filter((x) => x.id !== idBuah);
                console.log(newDataBuah);
                setDataHargaBuah(newDataBuah);
            });
    };

    return (
        <>
            <h1>Daftar Peserta Lomba</h1>
            <table>
                <thead>
                    <tr className="head">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataHargaBuah !== null &&
                        dataHargaBuah.map((item, index) => {
                            return (
                                <tr className="list" key={item.id}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{item.weight / 1000} kg</td>
                                    <td>
                                        <button
                                            value={item.id}
                                            onClick={handleEditForm}
                                        >
                                            edit
                                        </button>
                                        <button
                                            value={item.id}
                                            onClick={handleDelete}
                                        >
                                            remove
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </>
    );
};

export default BuahList;
