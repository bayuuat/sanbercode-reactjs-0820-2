import React from 'react';
import { BuahProvider } from './BuahContext';
import BuahList from './ListBuah';
import BuahForm from './FormBuah';

const DataHargaBuah = () => {
    return (
        <BuahProvider>
            <div className="holder">
                <BuahList />
                <BuahForm />
            </div>
        </BuahProvider>
    );
};

export default DataHargaBuah;
