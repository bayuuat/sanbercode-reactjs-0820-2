import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Routes from './Tugas-15/Routes';

const App = () => {
    return (
        <Router>
            <Routes />
        </Router>
    );
};

export default App;
